package dependency.code;

public class DepObject {
	private String subject = "", predicate = "", object = "";

	public boolean isUseless() {
		return subject.length() == 0 || predicate.length() == 0
				|| object.length() == 0;
	}

	public String toString() {
		return subject + predicate + object;
	}

	public String log() {
		return "subject:" + subject + "\n" + "predicate:" + predicate + "\n"
				+ "object:" + object + "\n";
	}

	public String getSubject() {
		return subject;
	}

	public String getPredicate() {
		return predicate;
	}

	public String getObject() {
		return object;
	}

	public void setSubject(String subject) {
		this.subject = subject.trim();
	}

	public void setPredicate(String predicate) {
		this.predicate = predicate.trim();
	}

	public void setObject(String object) {
		this.object = object.trim();
	}
}
